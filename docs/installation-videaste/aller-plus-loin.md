---
sidebar_position: 100
---
# Aller plus loin

Voici quelques outils et usages qui pourraient vous intéresser !

## Gérer des rendez-vous visio

Une des façons de monétiser votre travail, c'est de proposer à votre communauté des rendez-vous payants afin d'échanger de façon privilégier.

Il existe plusieurs services qui pourront mettre cela en place. 

[Setmore](https://setmore.com/fr) est celui qu'on vous recommande. Contrairement à Calendly, vous pourrez proposer sans frais des rendez-vous payants. (100 / mois, au delà, ça sera payant).