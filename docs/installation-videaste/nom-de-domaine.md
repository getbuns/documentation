---
sidebar_position: 2
---
# Création de votre nom de domaine

Chaque site a un nom de domaine : c'est son adresse internet qui contient un nom et une extension.

Exemple : `rouvegertbleu.tv`
- nom : `rougevertbleu`
- extension : `.tv`

Un nom de domaine est un **raccourci** vers une adresse (IP) d'un serveur.

## Enregistrer son nom de domaine

:::info
Pour avoir ton site Vidéaste, il va te falloir un nom de domaine. Pour cela il existe plusieurs services en ligne.

Nous te conseillons :
- [OVH](https://www.ovhcloud.com/fr/domains/) <sup>FR</sup> : inclu une adresse email gratuite (5Go)
- [Ionos](https://www.ionos.fr/domaine/noms-de-domaine) <sup>DE</sup> : inclu une adresse email gratuite (2Go)
- [Cloudflare](https://www.cloudflare.com/fr-fr/products/registrar/) <sup>US</sup> : domaine seul (connu pour être rapide et facile d'utilisation)
:::


## Sous-domaines

On peut en créer généralement autant qu'on veut à partir du moment où on a un nom de domaine.

:::info
Pour utiliser le site Vidéaste, il faudra te créer 2 sous-domaines :
- Un pour PeerTube, qui commencera par `peertube`.tonnomdedomaine.ext
- Un pour Mailgun, qui commencera pas `mg`.tonnomdedomaine.ext
:::

Exemple :
- `peertube.rougevertbleu.tv`
- `mg.rougevertbleu.tv`