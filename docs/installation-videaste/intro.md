---
sidebar_position: 1
---
# Introduction

Cette partie contient :
- Les étapes pour créer ton site Vidéaste.

Avant de te lancer, il est intéressant que tu es un aperçu de l'architecture glogal du projet qui utilises plusieurs briques et services pour fonctionner.

Le site est composé de deux outils principaux :
- [Ghost](#ghost) (**site**) outil libre _gratuit_
    - Thème "Video Maker" (**design**) thème libre _gratuit_
- [PeerTube](#peertube) (**player**) outil libre _gratuit_

Et tu vas devoir créer des comptes chez 3 fournisseurs de services :
- [Stripe](#stripe) (**paiement en ligne**) service payant _% sur le montant_.
- [Backblaze](#backblaze) (**stockage des médias**) service payant avec _offre gratuite_.
- [Mailgun](#mailgun) (**envoie des mails**) service payant avec _offre gratuite_.

Services supplémentaires _non indispensables_ :
- [Tally](#tally) (**formulaire de contact**) service gratuit avec _offre payante_.
- [Formspree](#formspree) (**formulaire de contact**) service payant avec _offre gratuite_.


## Ghost

[Ghost](https://ghost.org/), est un outil libre de blogging. C'est le site qui va présenter tes vidéos et les monétiser. C'est par ce site que ton audience va pouvoir s'abonner et accéder à ton contenu.

__Video Maker__ est le nom du thème Ghost, et donc du design, que nous avons créé pour adapter Ghost à la publication de vidéos.

## PeerTube

[PeerTube](https://joinpeertube.org/fr/), est un outil libre de diffusion de vidéo sur internet. Il permet d'uploader tes vidéos et de créer le player que tu pourras embarquer dans Ghost.

:::note
Le thème est compatible avec **PeerTube**, **Streamlike**, **Vimeo** et **YouTube**.
Pour utiliser un autre player que ceux mentionner ici, il faudra une petite mise à jour du thème pour bien prendre compte ce player.
:::

## Stripe

[Stripe](https://stripe.com/fr) est un service de paiement en ligne. Il permet aussi de gérer des dons, des abonnements, des produits, et de générer des factures.

**[Tarifs Stripe](https://stripe.com/fr/pricing)**
- 1,5 % + 0,25 € par paiement

## Backblaze

[Backblaze](https://www.backblaze.com/b2/cloud-storage.html) est un service d'hébergement de médias via des "Object Storage". Ca te permet de stocker tes vidéos, fichiers audio et images et d'avoir autant d'espace disque et de bande passante que nécessaire. Tu payes à la donnée. Si tu as très peu de vidéos et très peu de trafic, tu payeras proche de 0€ / mois.

**[Tarifs Backblaze](https://www.backblaze.com/b2/cloud-storage-pricing.html)**
- Stockage : $0,005 / Go / Mois (10 premiers Go, gratuits)
- Trafic : $0,01 / Go

:::note
Tu peux très bien utiliser un autre service d'Object Storage avec API S3. Backblaze est celui que l'on recommande par rapport à son tarif attractif et sa facilité de mise en place avec PeerTube.
:::

## Mailgun

[Mailgun](https://www.mailgun.com/) est un service d'envoie de mail. Tu payes à l'utilisation, ou avec abonnement.

**[Tarifs Mailgun](https://www.mailgun.com/pricing/)**
- $1 / 1000 mails (dont 1000 premiers mails offerts / mois)

:::note
Il existe également d'autres services d'envoie de mail. Il se trouve que Mailgun est celui choisi par Ghost.
:::

## Tally

[Tally](https://tally.so) est un service de formulaire. Il permet de créer des formulaires dont des formulaires de contact.

**[Tarifs Tally](https://tally.so/pricing)**
- envoies de formulaires illimités (logo Tally en bas des formulaires)

## Formspree

[Formspree](https://formspree.io/) est un service de formulaire. Il permet entre autre de créer un formulaire de contact sécurisé.

**[Tarifs Formspree](https://formspree.io/plans)**
- 50 envoies de formulaires offerts / mois