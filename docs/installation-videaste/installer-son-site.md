---
sidebar_position: 2
---
# Installer son site

## Installation de Ghost et Peertube

La première étape est d'installer Ghost et Peertube.

Nous allons prochainement proposer une installation en 1 click de Ghost + Peertube sur nos serveurs.
En attendant, vous pouvez nous contacter :

[📝 Demander une installation manuelle](https://tally.so/r/3yNarB).

:::info
Abonnez-vous au site bunseed.org pour être tenu au courant dès que le service mutualisé sera disponible :

[S'abonner à Demonopolisons.video](https://bunseed.org/#/portal/singin)
:::

Une fois l'installation faite, vous recevrez :
- vos identifiants de connexion Ghost et Peertube
    - ex : `admin`
    - ex : `password`
- l'adresse IP du serveur où se trouve votre Ghost et votre Peertube
    - ex : `23.58.657.21`


Mais avant de pouvoir installer Ghost et Peertube, il vous faudra créer :
- un nom de domaine
- un compte chez Mailgun
- un compte chez Backblaze
