---
sidebar_position: 3
---
# Création des comptes indispensables

## Mailgun (envoie de mails et newsletter)

Indispensable pour que Ghost puisse envoyer des mails et vos newsletters.

[📝 Créer un compte Mailgun](https://signup.mailgun.com/new/signup).

- Choisir le plan **`Foundation Trial`**

:::info
Pour pouvoir utiliser Mailgun, il faudra rentrer vos infos bancaires.
:::

:::caution
Dès votre inscription faites, sur votre compte Mailgun, cliquez sur **`Downgrade`**. Sinon vous serez facturé en début de mois prochain.
:::


## Backblaze (stockage médias)

Indispensable pour stocker et distribuer vos médias vidéos et photos.

[📝 Créer un compte Backblaze](https://www.backblaze.com/b2/sign-up.html).

- Choisir la région **`EU Central`**