---
sidebar_position: 4
---
# Création des comptes optionnels

## Stripe (paiements)

Indispensable si vous souhaitez proposer des abonnements payants.

[📝 Créer un compte Stripe](https://dashboard.stripe.com/register).

## Tally (formulaire de contact)

Permet de créer votre formulaire de contact et autres formulaires.

[📝 Créer un compte Tally](https://tally.so/signup).

## Formspree (formulaire de contact)

Autre solution pour créer votre formulaire de contact et autres formulaires.

[📝 Créer un compte Formspree](https://formspree.io/register).