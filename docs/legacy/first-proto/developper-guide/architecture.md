# Architecture

Si tu cherches à comprendre **comment s'articule techniquement le prototype**, tu es au bon endroit mon ami !

Les schémas de la page utilisent la notation [C4 model](https://c4model.com/).


## Diagramme de contexte - Vision Macro

Voici une vision macro de ce que nous construisons.

En bleu **le prototype**. C'est le code que nous développons ou les services open source que nous utilisons. 

En gris les différent systèmes externes utilisés par le prototype.

![C4 model - Context Diagram](/img/developer-guide/architecture/c4-context.svg) 

## Diagramme de conteneurs

Voici un zoom des différentes briques utilisées dans **le prototype** : 

:::tip Conteneur ?

Le terme *conteneur* fait référence à la définition du C4 model [Container Diagram](https://c4model.com/#ContainerDiagram) 

:::

![C4 model - Container Diagram](/img/developer-guide/architecture/c4-container.svg)

- `frontend` : Le site web consulté par les spectateur·trices. Il est unique pour chaque créateur·trice.    
    - Site statique généré via [gatsby](https://www.gatsbyjs.com/)
    - Embarque le serveur HTTP [caddy](https://caddyserver.com/) pour :
        - Fournir le site aux utilisateurs
        - Gérer le TLS
        - Piloter la regénération du site suite à une publication sur `cms-data`
    - Embarque un environnement `gatsby` (nodejs) pour régénérer le site
- `cms-data` : Le backoffice permettant aux créateur·trices de gérer le `frontend` 
    - CMS headless basé sur [strapi](https://strapi.io/)
    - Contient tous les contenus à afficher sur `frontend` (photos incluses)
    - Contient toute la configuration nécessaire pour la personnalisation de `frontend`
    - Gère les comptes utilisateurs (pour la restriction de certains contenus par exemple)
    - Gère les évènements envoyés par la solution de paiement (évènements de premier paiement, paiements récurrents, erreur lors d'un paiement, etc.)

## Accès au code

Tout le projet est disponible sur Gitlab à l'adresse : [https://gitlab.com/video-platform-demonopolisons](https://gitlab.com/video-platform-demonopolisons)

Lien vers les différents sous-projet :

- `frontend` : [https://gitlab.com/video-platform-demonopolisons/frontend](https://gitlab.com/video-platform-demonopolisons/frontend)
- `cms-data` : [https://gitlab.com/video-platform-demonopolisons/cms-data](https://gitlab.com/video-platform-demonopolisons/cms-data)
- `documentation` : [https://gitlab.com/video-platform-demonopolisons/documentation](https://gitlab.com/video-platform-demonopolisons/documentation)
    - code source du Handbook
- `delivery` : [https://gitlab.com/video-platform-demonopolisons/delivery](https://gitlab.com/video-platform-demonopolisons/delivery)
    - code source pour déployer **le prototype**
