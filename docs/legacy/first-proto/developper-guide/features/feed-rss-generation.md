---
sidebar_position: 2
---

# Génération de flux RSS

## Description 

La génération du flux RSS se base sur un plugin gatsby qui se charge de générer le fichier XML et de le déposer à la racine du site.

Les entrées du flux RSS sont récupérées par le `frontend` qui se connecte au `cms-data`. La génération du flux RSS se déroule uniquement lors de la phase du build du site statique. Pas de génération de flux en mode développement.

## Briques impliquées


1. frontend
1. cms-data

## Détail d'implémentation

### frontend

C'est le plugin officiel `gatsby-plugin-feed` qui est utilisé pour générer le flux [lien vers site](https://www.gatsbyjs.com/plugins/gatsby-plugin-feed/). 

:::tip Customisation du flux

La documentation du plugin n'est pas top. Mais ils donnent un lien vers la dépendances qu'ils utilisent pour générer le flux. Il s'agit du package npm [rss](https://www.npmjs.com/package/rss#itemoptions) qu'il faut consulter pour customiser le xml du flux.

:::

Pour plus de lisibilité dans le fichier `gatsby-config.js`, les flux sont externalisés dans `config/rss/`. 
Chaque fichier présent dans ce répertoire doit être inclus dans `gatsby-config.js` au niveau du `gatsby-plugin-feed`.

Liste des fichiers :

- pour le flux global : `all-article-rss-config.js`
- pour le flux podcast : `article-with-podcast-file.ts`

### cms-data

`cms-data` sert uniquement en tant que source pour récupérer les articles via une requête graphql.

