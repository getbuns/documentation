---
sidebar_position: 1
sidebar_label: Comment lire la documentation des fonctionnalités
---

# Fontionnalités

Les chapitres qui suivent permettent de comprendre dans le détail l'implémentation technique derrière chaque fonctionnalité. 
Autrement dit, vous trouverez ici le détail technique derrière chaque fonctionnalité listée dans [Le guide utilisateur](/docs/legacy/first-proto/user-guide/features/intro)).

Si tu cherches à modifier une fonctionnalité ou comprendre son fonctionnement, tu es au bon endroit !

Chaque fonctionnalité est détaillée suivant le template suivant :

* Description technique de la fonctionnalité
* Liste des briques impliquées
* Détail d'implémentation des différentes briques



