# Ajouter un article

Les articles sont affichés via leur miniatures sur les pages suivantes :

* Accueil
* Page listant les vidéos d'une catégorie. `/category/{slug}`
* Page des vidéos dans la partie droite dans les suggestions et les recommandations `/article/{slug}`

La consulation de l'article se fait via la page `/article/{slug}` où la partie gauche/centrale contient l'article.

## Briques impliquées

La publication d'article se fait simplement par la création d'un `article` dans `cms-data` puis de son affichage dans `frontend`.

1. cms-data
1. frontend


## Créer un article

Depuis l'interface de `cms-data` via le content-type `article` ou via l'API sur le endpoint `/articles`.

Au sein de l'API, la consulations des `articles` est public quand la création, modification, suppresison est soumis à authentification

### Afficher une vidéo

Pour afficher une vidéo, le `frontend` va récupérer la vidéo au moment du build puis utiliser le composant React `video.tsx`.

Ce composant se charge : 

- D'afficher la vidéo si celle-ci est `public`
- D'afficher la miniature pour la vidéo dont la visibilité est `membre` ou `premium` pour un utilisateur non connecté
- D'afficher la miniature pour la vidéo dont la visibilité est `premium` et que l'utilisateur est uniquemet `membre`
- D'afficher la vidéo pour la vidéo dont la visibilité est `membre` ou `premium` si l'utilisateur répond aux restrictions

:::tip Voir 

Vous pouvez consulter la page détaillant [les droits d'accès](https://liens-vers-la-bonne-page.xyz)

:::

Pour afficher une vidéo en fonction du fournisseur (youtube ou streamlike), on vérifie simplement si le champ `video_id` contient la chaine de caractère et on affiche le player `embed` associé :

* youtube, youtu.be, yt => youtube
* streamlike ou sl => streamlike 

