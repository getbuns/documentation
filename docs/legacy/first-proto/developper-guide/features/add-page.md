# Ajouter une page

L'ajout d'une page se fait simplement via l'ajout d'une entrée de type `page` dans `cms-data`.
C'est lors du build que `frontend` va générer la page avec ses composants.

On utilise le mécanisme de base de `gatsby` pour la création automatique des pages présentes dans le dossier `src/pages`.

## Briques impliquées

- `cms-data`
- `frontend`

## Ajout d'une liste d'articles 

### Résumé

Un liste d'articles est une zone dynamique (au sens [strapi](https://docs.strapi.io/user-docs/latest/content-types-builder/configuring-fields-content-type.html#dynamic-zones)) de l'entrée de type `page`.

Ces listes sont accessibles via le champ `dynamic_zone` de l'entrée `page`. C'est un tableau de [composants strapi](https://docs.strapi.io/user-docs/latest/content-types-builder/configuring-fields-content-type.html#components).
Actuellement, il n'y qu'un seul type de composant supporté : `list_articles_categories`

Une fois créé dans `cms-data`, le `frontend` récupère son contenu dans `src/pages/page/{StrapiPage.slug}.tsx` via graphql et construit la page avec ses composants.

### Détails

La page `src/pages/page/{StrapiPage.slug}.tsx` contient une requête graphql qui va récupérer les informations basiques de la page (son titre, son contenu, etc.) mais aussi une **partie** des `list_articles_categories`.

Une partie seulement ? Oui, uniquement les informations basiques du composants. La récupération des contenus (par exemple des articles associés à une catégorie) se fait dans un hook présents dans `src/hooks/`.

#### Problème lié à la récupération des contenus

Le composant `list_articles_categories` stockent le slug des catégories mais pas les articles, or on veut afficher à l'utilisateur les articles associés à ces catégories.
Naïvement, on pourrait se dire qu'il suffit de créé une requête graphql permettant de récupérer les articles associés aux catégories **sauf** que ce n'est pas possible avec `gatsby`. Les requêtes graphql avec variables sont disponibles uniquement pour lors de la création des pages. Il est possible d'utiliser `useStaticQuery` dans un composant React sauf que celui-ci n'accepte pas de paramètres, les requêtes graphql doivent être identiques pour chaque composant.

Quelques liens pour mieux comprendre la problématique :

- [Documentation Gatsby - Ajout de variables à la requête d'une page ](https://www.gatsbyjs.com/docs/how-to/querying-data/page-query/#how-to-add-query-variables-to-a-page-query)
- [Documentation Gatsby - useStaticQuery dans un composant](https://www.gatsbyjs.com/docs/how-to/querying-data/use-static-query/)

#### Contournement pour résoudre le problème

Pour récupérer les articles des catégories et appliquer les critères de tris et de nombre maximum, on utilise `src/hooks/use-articles-by-categories.ts` dont le rôle est de :

- Récupérer tous les articles du site via une requête graphql
- Trier et limiter le nombre maximum d'articles via du code `javascript`

C'est moins cool qu'une requête graphql paramétrable mais ça fait le job. À voir comment évoluent les performances au moment du build lorsque le nombre d'articles augmentent. Sachant que les requêtes graphql sont extraites puis cachés au moment du build, pas sûr que cela soit si gourmand que ça finalement.
