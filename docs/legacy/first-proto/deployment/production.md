# Production 

## En résumé

Ce chapitre décrit le fonctionnement et les étapes pour déployer **le prototype** sur le web ou pour le tester localement.

**En résumé, le prototype s'articule autour de conteneurs `docker` dont le lancement est orchestré par `docker-compose`. Les images des conteneurs sont stockées dans la partie registry de gitlab de chaque projet**

La liste des images `docker` de toutes les briques du prototype est disponible sur le [registry gitlab du projet](https://gitlab.com/groups/video-platform-demonopolisons/-/container_registries)

:::tip Quel est le rôle des différentes briques ?

Pour comprendre le rôle et les interactions entre les briques, n'hésite pas à consulter la page [Architecture]/docs/legacy/developper-guide/architecture)

:::

Voici la liste des services lors d'un déploiement réussi :

- `cms-data`
- `postgresql` en tant que base de données pour `cms-data`
- `frontend`


## Déploiement

Le déploiement est orchestré par un `docker-compose.yml` présent sur le projet [delivery](https://gitlab.com/video-platform-demonopolisons/delivery).

Pour déployer le prototype, exécuter les commandes suivantes sur le serveur :

    # Cloner le projet `delivery`
    $ git clone https://gitlab.com/video-platform-demonopolisons/delivery
    $ cd delivery
    
    # Récupérer les dernières version disponibles des images
    $ docker-compose pull

    # Mise en place des variables d'environnement via le fichier .env
    $ mv .env.example .env

    # Editer les variables d'environnement
    - DB_PASSWORD correspond au mot de passe de la base de données
    - CMS_DOMAIN correspond au nom de domaine de l'API (exemple: api.subdomain.example.xyz)
    - FRONTEND_DOMAIN correspond au nom de domaine du front (example subdomain.example.xyz)

    # Lancer le prototype
    $ docker-compose up -d

    # Pour suivre les logs
    $ docker-compose logs -f [SERVICE]

### Ports utilisés

Les ports `:80` et `:443` sont utilisés lors du lancement du prototype.

Le port `:80` redirige automatiquement vers le `:443`.

## Build

En cas de modification du code source, le build des images du projet est nécessaire.

Le build permet de publier les modifications de code dans une nouvelle version de l'image `docker` afin de pouvoir être déployée en local ou sur un serveur accessible depuis Internet.

Pour chaque projet du prototype le build consiste à :

1. Incrémenter la version de l'application dans le `package.json`
1. Construire l'image `docker` avec les tags `latest` et le numéro de version récupéré à l'étape précédente
1. Publier les images `docker` sur le registry gitlab de chaque projet

:::tip Build sur les poste de dev

Le build pour chaque projet se déroule sur un poste de développement pour le moment. Il est orchestré par un fichier `Makefile` présent à la racine du projet.

Pour le moment, on doit builder chaque projet indépendant.

:::

Pour lancer un build depuis sa machine vérifier que les outils suivants sont installés :

- `docker` version 20.10.x et supérieure
- `jq` version 1.6 et supérieure
- `make`
- `sed`
- Les droits pour mettre à jour le container registry de gitlab


### cms-data

La commande pour lancer le build est :

    $ make docker-increment-build-and-publish

:::tip Données par défaut

Pour simplifier l'utilisation, des données par défaut sont ajouté au tout premier lancement de `cms-data` lorsque la base de données est vide. 
Les données par défaut sont disponibles dans le fichier `app/bootstrap-data/data.json`

Pas d'ajout si la base contient déjà des données.

:::

### frontend

La commande pour lancer le build est :

    $ make docker-increment-build-and-publish
