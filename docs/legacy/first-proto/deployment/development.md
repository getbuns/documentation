# Développement

## En résumé

Pour développer sur le prototype, il faut installer les briques `cms-data` et `frontend` en local.

Pour plus de simplicité, les environnements de développement ont été conteneurisés via `docker`. Autrement dit :

- Tu peux modifier le code sur ta machine avec ton éditeur de code préféré
- Le code est synchronisé avec le conteneur
- C'est le conteneur qui se charge d'exécuter le code

:::tip Disclaimer

Toute la stack de développement a uniquement été testée sur un GNU/Linux. Donc il est possible que tu rencontres des soucis sur Mac ou Windows. N'hésite pas à remonter l'information si l'installation échoue.

Une fois l'installation effectuée, il ne devrait plus trop y avoir de différence entre les différentes machines de développement car tout est configuré dans les images `docker`.

:::

Le lancement des environnements de développement passe toujours par la même commande. Qu'importe le projet :

    $ make start

## Installation environnement de développement

Les commandes exécutées dans cette partie se font dans un terminal à partir du répertoire `~/code/video-platform/`
qui correspond à un répertoire dans lequel tout le code source des différents projet du prototype sera stockées. 
Libre à toi de déposer le code autre part ;)

### Pré requis

Vérifie que les outils suivants sont bien installés sur ta machine :

- `docker-compose` version 1.29.. et supérieure
- `docker` version 20.10.x et supérieure
- `git`
- `jq` version 1.6 et supérieure
- `make`

Ajouter les registry Gitlab à docker via `docker login`

    docker login registry.gitlab.com/video-platform-demonopolisons/cms-data -u <username> -p <token>
    docker login registry.gitlab.com/video-platform-demonopolisons/frontend -u <username> -p <token>

### Récupération du code source

`cms-data`

    git clone git@gitlab.com:video-platform-demonopolisons/cms-data.git


`frontend` 

    git clone git@gitlab.com:video-platform-demonopolisons/frontend.git

### Lancement de `cms-data`

Pour lancer l'environnement de développement `cms-data` : 

    cd cms-data && make start

Au tout premier lancement, la commande va : 

1. Récupérer les images `docker` du projet
1. Configurer la base de données en local
1. Ajouter des données de tests
1. Lancer les différents services : 
    - `strapi`
    - La base de données `postgres`

`cms-data` est désormais accessible via l'url [http://localhost:1337/admin](http://localhost:1337/admin). Au premier lancement, il faut créer un compte admin depuis l'interface web.

:::warning

Les données `postgres` ne sont pas conservées en cas de suppression des conteneurs. Autrement dit, en cas d'arrêt/relance du conteneur, les données sont conservées. En cas de suppression des conteneurs, les données sont effacées.


:::

Pour arrêter l'exécution de `cms-data`, `CTRL+C` doit être exécuté dans le terminal.

### Lancement de `frontend`

Dans un autre terminal (en laissant `cms-data` actif)

Pour lancer l'environnement de développement `frontend` : 

    cd frontend && make start

Cette commande va :

1. Récupérer l'image `docker` du projet
1. Lancer le conteneur

Une fois dans le conteneur, on peut indiquer à `gatsby` de lancer le mode développement du projet :

    yarn start
