---
sidebar_position: 1
sidebar_label: Introduction
---

# Déploiement - Introduction

Les chapitres qui suivent décrivent le déploiement du prototype en fonction des environnements :

- Pour initialiser un [environnement de développement]/docs/legacy/deployment/development) 
- Pour déployer la solution (en local ou sur le web) : [Déploiement production]/docs/legacy/deployment/production)
