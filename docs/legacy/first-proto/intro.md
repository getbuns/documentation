---
sidebar_position: 1
---

# Disclaimer

**Le prototype Strapi - Gatsby n'est plus maintenu.**

Il s'agit d'un prototype que nous avons développé au début du projet. Il nous a permis d'identifier les points importants pour nous et le projet et de tester quelques idées rapidement.


