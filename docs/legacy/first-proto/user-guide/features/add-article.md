# Ajouter un article

Une publication est actuellement nommée `article` au sein de l'outil.

Voici les fonctionnalités liées à la création/publication d'un article

## Créer un article

[[à compléter]]

### Lier une vidéo

Pour le moment, les vidéos ont besoin d'être hébergées en dehors de l'outil.

Les deux fournisseurs de vidéos supportés sont :

* Youtube
* Streamlike

Pour lier une vidéo à un article, il faut renseigner sont URL dans le champ `Video_id` (ajouter screenshot)

Deux modes pour remplir le champ `Video_id`:

1. Soit en renseignant l'url complète de la video
    - Pour youtube `https://www.youtube.com/watch?v=1p9_jKwEIj0` ou `https://youtu.be/1p9_jKwEIj0`
1. Soit en renseignant le format court :
    - pour youtube : `yt/{slug}` exemple : `yt/1p9_jKwEIj0`
    - pour streamlike : `sl/{slug}`

:::tip Streamlike au format court uniquement

Pour le moment, l'outil supporte uniquement le format court pour le provider Streamlike

:::
