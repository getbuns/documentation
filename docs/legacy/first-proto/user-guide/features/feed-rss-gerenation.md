# Génération de flux RSS

La plateforme permet de générer des flux RSS afin de permettre aux utilisateurs de suivre l'actualité du site. 

Pour le moment, uniquement la publication de nouveaux `Article` apparaît dans le flux RSS.

Il existe par défaut 2 flux RSS :

- Un flux global qui reprend tous les `Article`
- Un flux "podcast" qui reprend uniquement les `Article` contenant un fichier audio

La génération de flux RSS est automatique et n'est pas configurable pour le moment.
