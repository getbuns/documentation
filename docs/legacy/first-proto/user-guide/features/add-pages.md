# Ajouter une page

Pour ajouter une page, il faut ajouter une entrée de type `page` dans `cms-data`.

![screenshot de l'interface d'ajout de page](/img/user-guide/features/add-page/add-page.png)

Une page contient :

- Un titre
- Un contenu de type texte
- Un slug
- Un composant [SEO]/docs/legacy/user-guide/components/seo) (optionnel)
- Des listes de vidéos (optionnel)
Une fois créée, la page est accessible via `/page/{slug}`

:::tip Génération du slug

Le slug est généré automatiquement en fonction du titre de l'article, mais il est possible de le modifier si besoin. Pas de panique, si le slug est déjà utilisé l'interface indique une erreur.
Pas de risque de rendre une autre page indisponible ;)

C'est cet identifiant qui sera utilisé dans l'adresse de la page. 

:::

## Ajouter une liste d'articles 

Il est possible d'ajouter des composants à une page dont le rôle est d'afficher des articles en fonction d'une ou plusieurs catégories. 

![zone pour ajouter une liste d'articles](/img/user-guide/features/add-page/videos-list.png) 

Ces composants sont configurables et permettent :

- De définir le nombre max d'articles à afficher (par défaut 5)
- D'indiquer les catégories d'articles à afficher (optionnel. Si non renseigné, on récupère tous les articles sans se soucier de la catégorie)
- De définir le critère de tri (par défaut `date_desc`)
    - `date_desc` : tri les articles par date de publication. Du plus récent au plus ancien
    - `date_asc` : tri les articles par date de publication. Du plus ancien au plus récent 
    - `title_desc` : tri les articles en fonction de leur titre. Par ordre alphabétique inversé
    - `title_asc` : tri les articles en fonction de leur titre. Par ordre alphabétique
- D'ajouter un titre (optionnel)
- D'ajouter une description (optionnel)

Pour changer l'ordre d'apparition des composants dans la page, on peut utiliser la flèche en haut à droite du composant.
L'ordre d'affichage sur le site dépend de l'ordre d'affichage dans l'interface de `cms-data`.

:::tip Non duplication des articles

Dans le cas où un article apparaît plusieurs catégories, il ne s'affichera qu'une seule fois dans le composant.

:::
