# SEO

Le composant SEO permet d'optimiser le SEO pour les bots Google et les réseaux sociaux.

Il ajoute du code à la page pour améliorer le classement et l'affichage de la page sur les réseaux sociaux.

Le composant SEO contient :

- Un titre : metaTitle
- Une description: metaDescription
- une image: shareImage
