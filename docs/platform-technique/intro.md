---
sidebar_position: 1
---
# Introduction

Cette partie contient le descriptif de la stack technique de la plateforme que l'on développe.

On y retrouve : 

- [L'architecture technique](/docs/platform-technique/architecture) du projet
- [Le guide de développement](/docs/platform-technique/guide-dev) pour commencer à développer sur le projet
- Le guide de déploiement sur un serveur
