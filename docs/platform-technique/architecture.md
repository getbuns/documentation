---
sidebar_position: 2
---

# Architecture

Toute l'architecture technique du projet est décrite ici.

À noter que tous les noms de package/container font référence aux noms de l'architecture décrite ici.

:::info
Les schémas de cette page utilisent la modélisation [C4 Model](https://c4model.com/).
Les fichiers sources sont disponibles dans le répo du projet ainsi que dans l'image elle même (svg) et sont modifiables avec [Draw.io](https://draw.io)
:::

## Diagramme de contexte 

![Diagramme de contexte](/img/platform-technique/c4-model-context-diagram.svg)


### bunseed

Il s'agit de la brique principale. Bunseed est composé d'une série d'outil qui permet aux utlisateur·trics admin de gérer leur site.

## Diagramme de conteneurs

![Diagramme de conteneurs](/img/platform-technique/c4-model-container-diagram.svg)

### bunseed - blogging

Cette brique permet de gérer : 

* Les différents types d'abonnements payant ou gratuit
* La publication d'articles public ou restreint à un type d'abonnements
* D'envoyer une newslettr en fonction du type d'abonnement

C'est l'outil [Ghost](https://ghost.org/docs) qui est utilisé ici avec le thème Bunseed.
C'est le thème Bunseed qui contient une bonne partie des fonctionnalités.

### bunseed - core 

Cette brique permet d'ajouter des fonctionnalités à ``blogging``. On ne voulait pas trop modifier le fonctionnement de Ghost donc toutes les fonctionnalités qui ne sont pas inclus de base dans Ghost on été externalisée vers ``core`` et est inclus dans Ghost via des appels API.
