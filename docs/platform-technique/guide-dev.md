---
sidebar_position: 3
---
# Guide de développement

Tout le nécessaire pour lancer l'environnement de [`bunseed`](./architecture.md#bunseed) en mode développeur.

:::info
Tous les composants techniques sont décris dans [l'architecture](/docs/platform-technique/architecture) du projet
:::

## Organisation du repository

Tout le code `bunseed` se trouve sur le repository [https://gitlab.com/getbuns/bunseed.git](https://gitlab.com/getbuns/bunseed.git).

Pour le moment, il existe un sous-dossier pour :

* `core`
* `blogging`


## Pré requis 

Voici la liste des composants à installer pour lancer l'environnement de développement :

:::info
Les versions sont données à titre indicatif. C'est celle que j'utilise actuellement. Mais ça peut clairement fonctionner avec d'autres versions
:::

* `git`
* `docker` version 20.10.22+
* `docker compose` via plugin docker ou script version 2.15.1+
* `make` version 4.2.1+ qui est utilisé pour automatiser le lancement, la création des images docker, la configuration et l'installation des dépendances

## Get Started

### Initialisation et premier lancement 

Récupération des sources

    git clone https://gitlab.com/getbuns/bunseed.git 
    cd bunseed
    # Récupération des sources pour les sous-modules git
    git submodule init
    git submodule update

:::info
Pour pouvoir mettre à jour les submodule et `push` ses commits sur les repository, il faut récupérer
la branche principale.

Pour documentation (à lancer à la racine du projet)

    cd documentation && git fetch
    git checkout main 

Pour le thème ghost (à lancer à la racine du projet)

    cd blogging/ghost/content/themes/bunseed-ghost-theme && git fetch
    git checkout main

:::

On va maintenant configurer `stripe-cli` qui va servir de passerelle entre le site `stripe` et `blogging` en local.
Tous les flux venant de `stripe` vont être renvoyés vers `blogging`

Récupère la clé secrète sur [https://dashboard.stripe.com/test/apikeys](https://dashboard.stripe.com/test/apikeys) pour la mettre dans le fichier `.env` à la racine du projet.

    # le fichier doit ressembler à ça
    API_KEY=sk_test_Jtsetuidvteb75ec

Récupération en automatique de la clé secrète `stripe` pour communication avec `blogging`

    $ make stripe-save-webhook-secret

:::caution
Pour vérifier que la commande précédente s'est bien passée, il vérifier que la variable `WEBHOOK_SECRET` est bien dans fichier `.env`. Le fichier doit ressembler à :

    API_KEY=sk_test_Jtsetuidvteb75ec
    WEBHOOK_SECRET=whsec_STEARSTrstersutiersuiterst

En cas de problème => APPELER ROMAIN
:::

On installe les dépendances des différents composants (les librairies dans `node_modules`)

    make install-dependencies

On peut maintenant lancer l'environnement de développements

    make start

La commande `make start` (pour info, la commande ne rend pas la main, il faut ouvrir un autre terminal en cas de besoin) :

* Lance les conteneurs `docker` en se basant sur le fichier `compose.yml`
* Démarre le service `core` en mode dev qui tourne sur [strapi](https://docs.strapi.io)
* Démarre le service `blogging` qui tourne sur [ghost](https://ghost.org/docs)
* Démarre une commande `stipe-cli` pour connecter `stripe` et `blogging`
* Démarre le thème `video-maker` en mode dev
* Démarre `fake-smtp` pour simuler l'envoie des emails

`core` et `blogging` contiennent tous les deux un utilisateurs admin pré configuré :

* `core` - [http://localhost:8055](http://localhost:1337/admin) `admin@example` `d1r3ctu5`
* `blogging` - [http://localhost:2368/ghost](http://localhost:2368/ghost) `dev@demonopolisons.video` `demodemo1010`

Enfin, pour éviter de commit les updates de la base de données de `blogging` et les clés secrètes :

    git update-index --skip-worktree .env blogging/ghost/content/data/ghost-dev.db core/app/.tmp/data.db 

Une fois l'environnement de développement initialisé, plus besoin de lancer toutes ces commandes, il suffit le lancer la commande `make start` pour démarrer les différents composants.


:::info
Pense à connecter `blogging` et `stripe` depuis l'interface admin de `blogging` via [http://localhost:2368/ghost/#/settings/members?showPortalSettings=true](http://localhost:2368/ghost/#/settings/members?showPortalSettings=true) en cliquant sur le bouton en bas à gauche de `stripe`
:::


### Lancement de « tous les jours »

Une fois la phase d'initialisation terminée, plus besoin de relancer toutes les commandes précédentes.. Il suffit de lancer la commande `make start` à la racine du projet pour démarrer son environnement de développement.

Une fois lancé, on accède aux UI :


* `core` - [http://localhost:8055](http://localhost:1337/admin) `admin@example` `d1r3ctu5`
* `blogging` - [http://localhost:2368/ghost](http://localhost:2368/ghost) `dev@demonopolisons.video` `demodemo1010`
* `fake-smtp` - [http://localhost:8025](http://localhost:8025) *mails envoyés depuis `blogging` et `core` *

:::info
Pour les échanges de composant à composant au sein de l'environnement `compose`, des plusieurs alias existent.

* `core` --> `core` et `core.local`
* `blogging` --> `blogging` et `blogging.local`
:::

:::warning
Les emails réceptionné par `fake-smtp` sont supprimés à chaque lancement de l'environnement. Pas de persistance.
:::
