---
sidebar_position: 1
---
# Introduction

Cette partie contient :
- Un guide d'utilisation des spéficités du thème Ghost : Video Maker.

:::tip
Pour plus d'infos sur l'utilisation de base de Ghost, rendez-vous sur leur section **Ressources** : [ghost.org/resources/](https://ghost.org/resources/)
:::

:::caution
Il faut uploader ce fichier [routes.yaml](https://gitlab.com/video-platform-demonopolisons/video-maker-ghost-theme/-/raw/main/routes.yaml) dans _votredomaine.com_`/ghost/#/settings/labs` > `Routes`

![routes.yaml](/img/user-guide/labs-routes.yaml.png)
:::

:::info
Vous utilisez Ghost de façon autonome ? Vous pouvez télécharger le thème depuis :

[Thème Video Maker](https://gitlab.com/video-platform-demonopolisons/video-maker-ghost-theme/)
:::

