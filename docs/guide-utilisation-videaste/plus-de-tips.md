---
sidebar_position: 101
---
# Plus de tips

## Personnaliser le style

Vous pouvez personnaliser le style du thème Ghost Video Maker grâce au CSS.

Il suffit d'entrer du code CSS dans `Ghost > ⚙️ Settings > Code injection > Site header`

Par exemple, pour avoir une couleur de fond jaune dégueu :
```html
<style>
    body {
        background: yellow;
    }
</style>
```

Vous pouvez apprendre facilement le CSS par exemple ici : https://developer.mozilla.org/fr/docs/Learn/Getting_started_with_the_web/CSS_basics

## Ne pas utiliser Code injection > Site footer

~~`Ghost > ⚙️ Settings > Code injection > Site footer`~~

Le `{{Ghost Foot}}` est utilisé par le thème pour embarquer des données de durée et de date. Il est donc désactiver dans sa fonction native. Il reste en action seulement sur les pages et la page d'accueil.
