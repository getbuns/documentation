---
sidebar_position: 5
---
# Podcast audio

Vous pouvez utiliser votre site pour diffuser votre podcast audio.

Si vous utilisez Backblaze ou un autre service d'Object Storage, on vous conseille de mettre vos mp3 dessus.

## Mettre vos mp3 sur Backblaze

### Créer un Bucket (dossier) pour votre podcast

Dans votre administration de Backblaze :
- `Créer un bucket`
- Donnez lui un nom ex : `nompodcast`
- `Public`
- `Désactiver` la clé de chiffrement
- `Désactiver` la sécurité

Paramétrez les Règles CORS de votre nouveau Bucket :
- Cocher `Tout partager dans ce Bucket avec toute origine`
- Appliquez les règles CORS aux API suivantes: `Tous les deux`

### Uploadez vos mp3

Dans votre administration de Backblaze, sur votre Bucket :
- Cliquez sur le bouton `Charger/télécharger`
- Cliquez sur `Charger`
- Sélectionnez tous vos mp3

:::tip
Choisissez **128kbits** comme débit max pour votre mp3.
:::


### Récupérer l'URL de vos mp3

- Cliquez sur le fichier mp3 présent dans votre Bucket
- Copiez l'adresse du lien qui vous semble le plus joli `URL conviviale` ou `S3 URL`


## Référencer vos mp3 dans Ghost

Pour qu'un post Ghost contienne votre mp3, il faut ajouter son url dans : `Facebook Card` > `Facebook description`

- Allez dans l'éditeur de votre post Ghost
- Ouvrez la barre latérale settings en cliquant sur ![Settings](/img/user-guide/ghost-icons/post-settings.svg) situé en haut à droite
- `Facebook Card`
- Renseignez le **poids du mp3 en octets (bytes)** dans `Facebook title` (_facultatif_) ou laissez vide.
- Collez l'**URL du mp3** dans `Facebook description`

<details>
<summary>Récupérer le poids du mp3 en octets</summary>
<div>
<details>
<summary><b>Depuis votre ordi</b></summary>
    <ul>
        <li>Cliquez droit sur votre fichier mp3 / Propriétés</li>
        <li>Taille : XX,X Mo (<b>XX XXX XXX</b> octets)</li>
    </ul>
</details>
<details>
<summary>En ligne (plus long)</summary>
    <ul>
        <li>Entrez l'URL du mp3 dans votre navigateur.</li>
        <li>Cliquez droit / Inspecter</li>
        <li>Onglet Network</li>
        <li>Dans la liste à gauche, cliquez sur notre fichier mp3
            <ul><li>S'il n'apparait pas, rafraichir la page</li></ul>
        </li>
        <li>Onglet Headers</li>
        <li>Response Headers / Content-Length: <b>XXXXXXXX</b></li>
    </ul>
</details>
<details>
<summary>En ligne (rapide mais imprécis)</summary>
    <ul>
        <li>Dans Backblaze, il vous affiche la taille en Mo</li>
        <li>Faites XX,X Mo x 1 000 000 = <b>XX XXX XXX</b></li>
    </ul>
</details>
</div>
</details>

### Exemple de post avec mp3 renseigné

![Exemple de post avec mp3 renseigné](/img/user-guide/podcast-facebookcard.png)

:::info
Cela sacrifie le bon affichage d'un partage de votre publication sur Facebook. Nous sommes partis de ce qui est pratiqué et conseillé par Ghost.

Le renseignement de vos podcast audio sera amélioré dans une prochaine version ;)
:::

## N'oubliez pas le tag #podcast

Pour spécifier qu'une publication est un podcast, il faut lui ajouter le tag `#podcast`.

:::info
Pour bien être pris en compte comme podcast, le tag `#podcast` doit avoir comme slug `hash-podcast`. C'est le slug généré automatiquement quand on crée le tag.
:::

## Votre flux RSS

Votre flux RSS de Podcast existe à cette adresse : _votredomaine.com_`/feed/podcast/`

C'est cette URL qu'il faut fournir aux différentes plateformes de podcasts comme Spotify, Apple Podcast, Deezer etc.

:::caution
Votre flux RSS n'existera à cette URL que si vous avez bien uploader ce fichier [routes.yaml](https://gitlab.com/video-platform-demonopolisons/video-maker-ghost-theme/-/blob/main/routes.yaml) dans _votredomaine.com_`/ghost/#/settings/labs` > `Routes`

![routes.yaml](/img/user-guide/labs-routes.yaml.png)
:::