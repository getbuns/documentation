---
sidebar_position: 8
---
# Page Liens

Le thème Ghost Video Maker permet de créer une page de liens, comme peut le faire LinkTree.

## Créer des boutons

Pour cela, il suffit de créer une page avec comme template `Links`

![Template Links](/img/user-guide/links-template.png)

Pour créer des gros liens cliquables, ajouter des boutons (ils seront stylisés à la Linktree).

![Créer un bouton](/img/user-guide/links-button.png)

## Carrousel des dernières publications

En bas de votre page lien est affiché un petit carrousel des dernières publications.

## Ajouter un fond d'écran

La vignette (feature image) de la page template Links sera utilisé comme fond d'écran de la page.

## Liens sur 2 colonnes

Vous pouvez mettre vos liens sur 2 colonnes.

Pour se faire, il faut entourer les liens que vous souhaitez être mis sur 2 colonnes avec des blocks HTML :

```HTML
<div class="links-grid-2">
```

**Ici vos boutons de liens**

```HTML
</div>
```
