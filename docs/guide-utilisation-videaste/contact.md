---
sidebar_position: 100
---
# Contact

Plusieurs options s'offrent à vous pour que l'on puisse vous écrire.

## Direct par mail (déconseillé)

Vous pouvez créer un lien dans votre menu qui redirige le visiteur vers sa boite de messagerie pour vous envoyer un mail.

`mailto:votreadresse@mail.com`

:::danger
En inscrivant votre adresse email publiquement sur votre site, vous êtes la cible des robots spammeurs et vous risquez de recevoir énormément de Spam...

Si vous souhaitez utiliser cette solution, rendez-vous à la prochaine section "Direct par mail (protégé)
:::

## Direct par mail (protégé)

Vous pouvez créer un lien dans une page à l'aide d'un code javascript à mettre dans un bloc HTML. Ce javascript va encoder votre adresse email et ainsi la protéger.

Plusieurs services en ligne vous aide à générer ce code dont :
- [email-encoder.com](https://www.email-encoder.com/)


## Formulaire via Tally.so

Créer un nouveau formulaire depuis votre compte Tally.

:::tip
Vous pouvez utiliser ce template adapté au thème Video Maker : https://tally.so/templates/ghost-video-maker-contact/mKp2z3
:::

:::caution
Dans les `Settings` du formulaire Tally, pensez à activer `Self email notifications` pour recevoir les envoies dans votre boite mail.
:::

Ensuite cliquez sur `Publish`.


### Intégrer le formulaire dans votre site

Plusieurs options existent pour intégrer le fomulaire sur votre site.

Vous pouvez les explorer dans Tally en cliquant sur `Share`.

#### Share Link

Vous pouvez copier cette url et l'utiliser dans votre menu.

#### Embed standard

Vous pouvez créer un code d'embed HTML que vous pourrez inscrire dans une page Contact à créer dans votre Ghost.

#### Embed popup

Vous pouvez afficher une popup du formulaire quand on clique sur le lien du menu.

Pour cela, dans Ghost il faudra aller dans Settings `⚙️` > `Code injection`

![Code injection](/img/user-guide/settings-codeinjection.png)

Collez dans `Site header` ce code :

    <script async src="https://tally.so/widgets/embed.js"></script>


Ensuite ajoutez un lien contact dans votre menu Ghost. Allez dans Settings `⚙️` > `Navigation`

Créez un lien dans votre PRIMARY NAVIGATION et/ou SECONDARY NAVIGATION avec comme URL :

    https://tally.so/r/[ID]#tally-layout=modal&tally-width=480&tally-open=[ID]

Remplacer `[ID]` par l'id de votre Tally que vous pouvez trouver dans le Share Link de Tally :

![Tally ID](/img/user-guide/tally-id.png)




## Formulaire via Formspree.io

Formspree permet de créer un formulaire avec un code d'embed HTML à inscrire dans une page Contact à créer dans votre Ghost.

<details>
<summary>Pour que le formulaire corresponde au style de Ghost, vous pouvez intégrer dans un bloc HTML dans votre page, ce style CSS :</summary>
<div>

    <style>
    form input,
    form select,
    form textarea,
    form fieldset,
    form optgroup,
    form label,
    .StripeElement {
    font-family: inherit;
    font-size: 100%;
    color: inherit;
    border: none;
    border-radius: 0;
    display: block;
    width: 100%;
    padding: 0;
    margin: 0;
    -webkit-appearance: none;
    -moz-appearance: none;
    }
    form label,
    form legend {
    font-size: 1em;
    margin-bottom: 0.5rem;
    }
    /* border, padding, margin, width */
    form input,
    form select,
    form textarea,
    .StripeElement {
    box-sizing: border-box;
    border: 1px solid rgba(0, 0, 0, 0.2);
    background-color: rgba(255, 255, 255, 0.9);
    padding: 0.75em 1rem;
    margin-bottom: 1.5rem;
    border-radius: 5px;
    }
    form input:focus,
    form select:focus,
    form textarea:focus,
    .StripeElement:focus {
    background-color: white;
    outline-style: solid;
    outline-width: thin;
    outline-color: gray;
    outline-offset: -1px;
    }
    form [type="text"],
    form [type="email"],
    .StripeElement {
    width: 100%;
    }
    form [type="button"],
    form [type="submit"],
    form [type="reset"] {
    width: auto;
    cursor: pointer;
    -webkit-appearance: button;
    -moz-appearance: button;
    appearance: button;
    }
    form [type="button"]:focus,
    form [type="submit"]:focus,
    form [type="reset"]:focus {
    outline: none;
    }
    form select {
    text-transform: none;
    }
    #my-form-button {
    color: var(--white-color);
    background: var(--ghost-accent-color);
    border: 0;
    padding: 6px 18px;
    border-radius: 6px;
    cursor: pointer;
    }
    </style>

</div>
</details>