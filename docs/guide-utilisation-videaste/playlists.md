---
sidebar_position: 4
---
# Playlists

## Tag = Playlist

Chaque tag génèrera une playlist.

Si un post possède un ou plusieurs tags, sur sa page, en dessous de la description de la vidéo, est généré un carrousel playlist du premier tag auquel il appartient.

:::info
Les **hashtag** (ex: `#podcast`) ne génèrent pas de playlist.
:::

:::caution Attention
Les **carrousels playlists sous une vidéo** sont générés à partir du premier tag de la vidéo. Même si une vidéo a plusieurs tags, elle n'affichera la playlist que de son premier tag.

- Tags : `News Sketchs #podcast` ✅ = La vidéo affichera une playlist carrousel de toutes les vidéos ayant comme tag `News`

- Tags : `#podcast News Sketchs` ⛔ = La vidéo n'affichera pas de playlist 
:::



## Créer une page Playlists

Vous pouvez créer une page qui affiche toutes vos playlists en créant une page et en lui attribuant le template `Tags`.

![Template Tags](/img/user-guide/tags-template.png)


## Personnalisez vos playlists

Vous pouvez attribuer une **description**, une **couleur** ou une **image** à chacune de vos playlists.

Pour cela, il suffit d'aller dans l'onglet Tags `Ghost > Tags` et d'éditez vos tags.

![Tags](/img/user-guide/ghost-tags.png)