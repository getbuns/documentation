---
sidebar_position: 7
---
# Page Historique

Le thème Ghost Video Maker enregistre l'état d'avancement de lecture des vidéos et crée alors également un historique de visionnage.

Vous pouvez créer une page qui viendra recenser les vidéos regardées et leur état d'avancement.

Il faut créer une page avec comme template `History`.

![Template History](/img/user-guide/history-template.png)

Vous pouvez nommer votre page comme vous le souhaitez et y mettre la description que vous souhaitez.

:::tip
Titre : 

    Mon historique

Description :

    Ici votre historique de lecture des vidéos.
    Conservé sur ce navigateur seulement.

:::

:::info
L'historique de lecture n'est pas relié à un compte utilisateur, mais enregistré dans le navigateur seulement.

Il sera relié à un compte utilisateur dans une prochaine version ;)
:::