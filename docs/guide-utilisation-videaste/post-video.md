---
sidebar_position: 3
---
# Post Vidéo

Il y a plusieurs éléments à connaître pour bien gérer ses pages de vidéos.

## Gérer vos pages vidéos

`Ghost > ⚙️ Settings > Design`

<details>
  <summary>Plus d'infos sur comment se rendre sur la page Design</summary>
  <div>
Aller dans l'administration de votre Ghost :

- _votredomaine.com_`/ghost/`
- cliquez sur `⚙️` ("settings" en bas à gauche)
- cliquez sur `Design`

Ou accédez directement avec cette url :

- _votredomaine.com_`/ghost/#/settings/design`

![Settings](/img/user-guide/home-settingstodesign.png)
  </div>
</details>

A gauche vous avez l'onglet :


### Post

- **Donation url** : Vous permet d'activer le bouton 🟥 de don sous vos vidéos. Il vous suffit de renseigner une URL. Par exemple une URL de paiement de don créé avec Stripe.
    - ex : `https://donate.stripe.com/xxxxxxxxxx`

![Icones](/img/user-guide/post-actions.png)

- **Comment url** : Vous permet d'activer le bouton 🟩 de commentaire sous vos vidéos. Il vous suffit de renseigner une URL. Par exemple une URL de formulaire Tally.so.
    - ex : `https://tally.so/#tally-open=xxxx`

:::tip
Vous pouver utiliser ce template de Tally adapté au thème Video Maker :
https://tally.so/templates/ghost-video-maker-comment/mBEo4n 
:::

- **Show author** : Affiche ou pas l'auteur / les auteurs de la vidéo tout en bas de la page.

- **Show related posts** : Affiche ou pas un carrousel en bas des pages de vos vidéos pour recommander d'autres vidéos.


## Player vidéo

Pour le moment le thème est optimisé pour fonctionner avec le player de PeerTube.

:::info
Pour apprendre à utiliser PeerTube, rendez-vous sur la [documentation de PeerTube](https://docs.joinpeertube.org/fr/).
:::

Nous allons voir comment intégrer un player.

### Depuis PeerTube

Sur la page de lecture de votre vidéo PeerTube, on va récupérer le code HTML d'embarquement, l'embed d'iframe :
- Cliquez sur `PARTAGER` situé sous le player.
- Puis sur `Intégration`.
- Puis sur `COPY` (situé à droite du champ `<iframe title="..." width="560" ...`)

![Peertube Embed](/img/user-guide/post-peertube-embed.png)

:::info
**Le format d'image (ratio)** du player (de base en 16/9), est déterminé par ses dimentions présentes dans le code embed :

    width="560" height="315"

Exemple : Si votre vidéo est en format scope (2.35) vous pouvez dimensionner votre player au même format :

    width="235" height="100"

Le player conservera alors le ratio renseigné tout en étant responsive, en s'adaptant à la taille de l'écran.
:::

### Dans Ghost

Dans l'éditeur de votre Post Ghost, on va coller le code HTML de l'iframe :
- Cliquez sur `+` ou tappez `/` dans l'éditeur.
- Choisissez `HTML`.
- Collez ce que vous avez copié précédemment (`<iframe title="..." width="560" ...`)

![HTML](/img/user-guide/post-html.png)


## Mettre un teaser

Pour les publications restreintes aux membres ou abonnés, vous pouvez mettre un teaser.

Il vous suffit de créer une séparation "Public preview" dans votre post Ghost et de mettre l'embed du teaser au dessus.
- Cliquez sur `+` ou tappez `/` dans l'éditeur.
- Choisissez `Public preview`.
- Au dessus de la ligne, rajouter un bloc `HTML` et collez le code `<iframe...` de votre tesaser.
- Placez bien le player de votre vidéo exclusive aux abonnés, sous la ligne `FREE PUBLIC PREVIEW`.

![Public preview](/img/user-guide/post-publicpreview.png)

### Exemple de post avec Teaser

![Exemple de post avec Teaser](/img/user-guide/post-teaser.png)

:::tip
Mettre le texte descriptif (ou juste son intro) au dessus de la ligne "Public preview". Ainsi il sera affiché à vos visiteurs non abonnés.
:::

:::info
S'il n'y a pas de Teaser vidéo, c'est la vignette qui sera affichée à la place.
:::



## Mettre un chat pour les lives

Vous pouvez ajouter un chat à droite de votre player vidéo, simplement en ajouter le code embed du chat sous votre player vidéo.

Vous pouvez utiliser le chat de PeerTube ! Pour cela, il faut installer son plugin.
- Aller sur votre PeerTube
- `⚙️ Administration` situé en haut à gauche
- `Plugins/Thèmes`
- `Recherche` et `Plugins`
- Installez le plugin `livechat`

Créer un live PeerTube et récupérer le code embed du Chat.
- `Publier` situé en haut à droite
- Onglet `Aller au direct`
  - si l'onglet n'est pas disponible, il faut activer le direct en allant `⚙️ Administration > Configuration > Diffusion en direct`, `Activer la diffusion en direct`
- Une fois le direct créé, aller sur sa page de lecture et vous verrez le chat à droite du player.
- Cliquez sur le bouton `🔗`
- Décochez `Lecture seule`
- `COPIER` le code `<iframe...`
- Collez le dans un bloc HTML de votre publication Ghost, sous le player du direct.

![Chat Peertube Embed](/img/user-guide/post-peertube-chat-embed.png)

### Exemple de post avec Chat

![Exemple de post avec Teaser et Chat](/img/user-guide/post-teaser-chat.png)



## Etiquette : En direct #live

Pour spécifier qu'une vidéo est un live, il suffit de lui rajouter comme tag `#live`.

:::info
Pour bien faire apparaître l'étiquette **En direct**, le tag `#live` doit avoir comme slug `hash-live`. C'est le slug généré automatiquement quand on crée le tag.
:::

## Etiquette : Avant-première #preview.

Pour spécifier qu'une vidéo est en avant-première, il suffit de lui rajouter comme tag `#preview`.

:::info
Pour bien faire apparaître l'étiquette **Avant-première**, le tag `#preview` doit avoir comme slug `hash-preview`. C'est le slug généré automatiquement quand on crée le tag.
:::

## Format affiche #poster

Les vignettes de vos vidéos sont de base en format 16/9. Vous pouvez les mettre en format vertical comme une affiche poster de film, en lui mettant le tag `#poster`.

:::info
Pour bien avoir le format vertical du poster, le tag `#poster` doit avoir comme slug `hash-poster`. C'est le slug généré automatiquement quand on crée le tag.
:::


## Préciser la durée de vos vidéos

Pour afficher sur vos vignettes la durée des vidéos, il faudra l'ajouter manuellement en l'inscrivant dans le `Post footer`.
- Allez dans l'éditeur de votre post Ghost
- Ouvrez la barre latérale settings en cliquant sur ![Settings](/img/user-guide/ghost-icons/post-settings.svg) situé en haut à droite
- `Code injection`
- Dans `Post footer` inscrivez la durée de la vidéo sous le format `hh:mm:ss`.

Exemple pour une vidéo d'1h 20min et 30sec : `01:20:30`.

![Durée](/img/user-guide/post-duration.png)


:::info
La durée de la vidéo sera récupérée de façon automatisée dans une prochaine version ;)
:::

## Afficher une vidéo programmée dans le futur

Vous pouvez afficher sur votre site une vidéo qui sera disponible plus tard. Pour cela vous devez renseigner la date et l'heure de sortie de la vidéo dans `Code injection` > `Post footer`.
- Renseignez sous le format `date,aaaa-mm-jj hh:mm`.

Exemple pour une vidéo qui sort le 1er décembre 2023 à 18h dont la durée est d'1h 20min et 30sec :

    date,2023-12-01 18:00
    duration,01:20:30
