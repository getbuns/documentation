---
sidebar_position: 6
---
# Page Ma Liste

Le thème Ghost Video Maker permet à vos visiteurs de mettre vos vidéos dans une liste. Cela leur permet de les mettre en favoris et de les regarder plus tard.

Pour que cette fonctionnalité fonctionne, il faut créer une page dont le slug doit être `my-list`.

![Slug my-list](/img/user-guide/mylist-slug.png)

Et dont le template doit être `Mylist`.

![Template Mylist](/img/user-guide/mylist-template.png)

Vous pouvez nommer votre page comme vous le souhaitez et y mettre la description que vous souhaitez.

:::tip
Titre : 

    Ma liste

Description :

    Ici les vidéos que vous avez sauvegardées dans votre liste.
    Liste enregistrée dans ce navigateur seulement.

:::

:::info
Les vidéos enregistrées dans la liste ne sont pas reliées à un compte utilisateur, mais enregistrées dans le navigateur seulement.

Elles seront reliées à un compte utilisateur dans une prochaine version ;)
:::