---
sidebar_position: 2
---
# Page d'Accueil

Il y a plusieurs éléments à connaître pour bien gérer sa page d'accueil.

## Gérer votre page d'accueil

`Ghost > ⚙️ Settings > Design`

<details>
  <summary>Plus d'infos sur comment se rendre sur la page Design</summary>
  <div>
Aller dans l'administration de votre Ghost :

- _votredomaine.com_`/ghost/`
- cliquez sur `⚙️` ("settings" en bas à gauche)
- cliquez sur `Design`

Ou accédez directement avec cette url :

- _votredomaine.com_`/ghost/#/settings/design`

![Settings](/img/user-guide/home-settingstodesign.png)
  </div>
</details>


A gauche vous avez plusieurs onglets :

### Brand

- **Site description** : vous permet de changer la description de votre site qui apparait dans les moteurs de recherche
- **Accent color** : La couleur clé de votre site
- **Publication icon** : Votre logo qui s'affichera dans la barre d'adresse des navigateurs
- **Publication logo** : Votre logo qui s'affichera en haut à gauche de votre site
- **Publication cover** : Une image qui s'affichera quand votre site est partagé sur les réseaux

### Site-wide

- **Navigation layout** : la disposition de votre en-tête de site
- **Title font** : La police d'écriture de vos titres
- **Body font** : La police d'écriture de vos textes
- **White logo for dark mode** : Votre logo pour la version sombre du thème **_Non pris en charge pour le moment_** 

### Homepage

Ici nous allons décider des différents carrousels/playlists à afficher en première page.

- **Show featured posts** : Affiche ou pas vos publications mises en avant, mis en "Feature this post" quand vous éditez un "post".

- **Show last posts** : Affiche ou pas un carrousel de vos dernières vidéos publiées.

- **Tags to display** : Affiche ou pas des playlists pour chaque slug de tag renseigné, séparés par des virgules.
  - Pour trouver les "slug" de vos tags, aller sur votre page Tags dans l'admin de Ghost : _votredomaine.com_`/ghost/#/tags`, et vous verrez une colone `SLUG`.
  - Ex : `vlog,podcast,news`

- **Show these authors post** : Affiche ou pas des playlists pour chaque slug d'ami renseigné, séparés par des virgules.
  - Pour trouver les "slug" de vos authors, aller sur votre page Tags dans l'admin de Ghost : _votredomaine.com_`/ghost/#/settings/staff`, et cliquez sur l'auteur que vous souhaitez, vous verrez un champ de texte `Slug`.
  - Ex : `guillaume,romain`

:::info
Dans ce thème Ghost, nous utilisons le staff pour ajouter des "Amis" et recommander les contenus de vos amis.

Pour inviter un ami, il vous suffit d'aller sur _votredomaine.com_`/ghost/#/settings/staff` et de cliquer en haut à droite sur `Invite people`

Entrer son adresse email et laissez le rôle à `Contributor`.

Une fois le compte créé par votre ami, pensez à bien renseigner l'url de son site et son avatar.
:::

- **Show all authors posts** : Affiche ou pas un seul carrousel mélangeant les vidéos de vos amis.
  - `no (default)` : n'affiche pas le carrousel
  - `random` : arriche le carrousel en mélangeant les vidéos
  - `desc` : affiche le carrousel en triant les vidéos de la plus récente à la plus ancienne
  - `asc` : affiche le carrousel en triant les vidéos de la plus acienne à la plus récentre
Cela affichera également ce carrousel en bas des pages de vos vidéos.

- **Show these authors** : Affiche ou pas un carrousel des avatars de vos amis. Renseignez les slugs de vos amis séparés par des virgules.
  - Ex : `guillaume,romain`
  - Ex : `all` les affiche tous

:::caution
**Pour que vos amis et leurs vidéos s'affichent bien, il faudra les mettre en auteur d'au moins une publication.**

Pour cela, on vous invite à créer une page vide que vous pouvez nommer `Staff amis`, et à droite, vous pouvez rajouter tous les amis que vous voulez activer.
:::



## Entête de votre page d'accueil

Vous pouvez ajouter ce que vous voulez en haut de votre page d'accueil.

Pour cela, créez une nouvelle page que vous nommerez `Home` et qui doit avoir comme slug `home`.

Le contenu de cette page s'affichera sur votre page d'accueil avant toutes vos playlists.