---
sidebar_position: 1
---

# Bienvenue

:::caution documentation en construction

Ce handbook est encore en construction.
:::


Vous êtes sur le Handbook de la version beta « Bunseed ».

Cette documentation s'inspire de l'approche [Why handbook first](https://about.gitlab.com/handbook/handbook-usage/#why-handbook-first) de Gitlab. 

Ce site est généré avec [Docusaurus](https://docusaurus.io/) et est disponible à l'adresse [https://docs.bunseed.org](https://docs.bunseed.org). 

Son déploiement est automatique à chaque fois qu'un changement est poussé sur la branche `master`.

# Pourquoi un Handbook

L'idée est de ne pas multiplier les sources d'informations. Tout ce qu'on doit savoir sur le projet se trouve ici.
Tant pour les parties-prenantes du projet que les utilisateurs. 
De l'installation de l'environnement de développement, à l'utilisation de l'outil en passant par l'organisation du projet, tout y est.

L'idée est aussi de concevoir ce prototype en commençant **toujours** par écrire dans le handbook avant de développer quoi que soit.

On prépare une page pour expliquer comment contribuer à ce Handbook si vous voulez participer.

Quelques exemples d'Handbook de qualité :

* [Athens Handbook](https://handbook.athensresearch.org/)
* [Posthog](https://posthog.com/handbook)
* [Mattermost](https://handbook.mattermost.com)
* [Carpentries](https://docs.carpentries.org/index.html)
* [Gitlab](https://about.gitlab.com/handbook/)

