---
sidebar_position: 2
---
# À propos du projet Bunseed

Le projet Bunseed est dans un premier temps de se rassembler, nous créatrices et créateurs de contenus média sur Internet autour des questions de la diffusion en ligne.

L’idée est de pouvoir partager nos réflexions, nos tentatives, et nos histoires sur les choix de diffusion et de monétisation que nous faisons. Où est-ce qu’on diffuse nos contenus ? Avec quels outils ? Quels choix de monétisation faisons-nous ? Avec quels outils ?

Ce site nous sert aussi à référencer toutes les initiatives d’autodiffusion que nous connaissons. Vous pouvez retrouvez la liste sur la page [Références](https://bunseed.org/references/).

Nous rêvons d’un site pour créateur-rice-s qui soit la propriété de celles et ceux qui créent, qui soit facile à mettre en place, à entretenir et à faire évoluer. Un site pour être pleinement autonome et indépendant de tous les intermédiaires que l’on connait aujourd’hui, le tout en interconnectant les sites et les contenus entre eux.


## Une version beta fonctionnelle !

Depuis Novembre 2023, il est possible de profiter de la première version de l'outil pour vidéaste qui permet de rassembler sa communauté, diffuser et monétiser son contenu.

## À qui est-ce destiné ?

C'est destiné aux créatrices et créateurs de vidéos et podcast audio.

## La vision

Les communautés rassemblés sur les réseaux sociaux n'appartiennent qu'aux réseaux sociaux. Il est temps de sécuriser sa communauté en lieu sur, c'est à dire sur un outil qui appartient désormais aux créateurs.

## Comment nous contacter ?

Pour toute question lié au projet, vous pouvez nous écrire à <a href="mailto:contact@bunseed.org">contact@bunseed.org</a>

## L'équipe

Guillaume - Romain 
