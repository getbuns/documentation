// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

const config = {
    title: 'Bunseed - Documentation',
    tagline: 'Outils open source pour partager vos vidéos et gérer votre communauté',
    url: 'https://docs.bunseed.org/',
    baseUrl: '/',
    onBrokenLinks: 'throw',
    onBrokenMarkdownLinks: 'warn',
    favicon: 'img/favicon.png',
    organizationName: 'getbuns', // Usually your GitHub org/user name.
    projectName: 'Bunseed', // Usually your repo name.
    scripts: [
        {
            src: 'https://plausible.io/js/plausible.js',
            defer: true,
            'data-domain': 'docs.bunseed.org'
        }
    ],
    plugins: [require.resolve("docusaurus-plugin-image-zoom")],
    presets: [
        [
            'classic',
            ({
                docs: {
                    sidebarPath: require.resolve('./sidebars.js'),
                    // Please change this to your repo.
                    editUrl: 'https://gitlab.com/video-platform-demonopolisons/documentation/-/blob/master/',
                },
                theme: {
                    customCss: require.resolve('./src/css/custom.css'),
                },
            }),
        ],
    ],

    themeConfig:
        ({
            navbar: {
                title: 'Bunseed - Documentation',
                logo: {
                    alt: 'My Site Logo',
                    src: 'img/favicon.png',
                },
                items: [
                    {
                        type: 'doc',
                        docId: 'intro',
                        position: 'left',
                        label: 'Handbook',
                    },
                    {
                        href: 'https://gitlab.com/getbuns/documentation',
                        label: 'Gitlab',
                        position: 'right',
                    },
                ],
            },
            footer: {
                style: 'dark',
                links: [
                    {
                        title: 'Docs',
                        items: [
                            {
                                label: 'Handbook',
                                to: '/docs/intro',
                            },
                        ],
                    },
                ],
                copyright: `Copyright © ${new Date().getFullYear()} Bunseed. Built with Docusaurus.`,
            },
            prism: {
                theme: lightCodeTheme,
                darkTheme: darkCodeTheme,
            },
            zoom: {
                selector: '.markdown :not(em) > img',
                config: {
                    // options you can specify via https://github.com/francoischalifour/medium-zoom#usage
                    background: {
                        light: 'rgb(255, 255, 255)',
                        dark: 'rgb(50, 50, 50)'
                    }
                }
            },
        }),
};

module.exports = config;
