import React from 'react';
import clsx from 'clsx';
import styles from './HomepageFeatures.module.css';

const FeatureList = [
    {
        title: 'Auto-diffusion',
        Svg: require('../../static/img/undraw_docusaurus_mountain.svg').default,
        description: (
            <>
                Un des objectifs de l'outil <strong>Bunseed</strong> est de permettre à chaque créateur de disposer d'un outil simple
                pour publier ses vidéos au travers d'un site web.
            </>
        ),
    },
    {
        title: 'Fédération',
        Svg: require('../../static/img/undraw_docusaurus_tree.svg').default,
        description: (
            <>
                Avoir un site pour publier ses vidéos c'est cool, mais si ce site est perdu dans les lymbes du web, ça n'a pas d'intérêt.
                C'est pourquoi, tous les sites utilisant notre outil sont reliés entre eux pour former un catalogue global.
            </>
        ),
    },
    {
        title: 'Reprendre le contrôle',
        Svg: require('../../static/img/undraw_docusaurus_react.svg').default,
        description: (
            <>
                Nous ne voulons pas recréer une enième plateforme ou un énième réseau social. Avec <strong>Bunseed</strong> chaque site est indépendant.
                C'est vous qui choisissez votre modèle de monétisation, qui avez accès aux informations de vos abonnés. Vous pouvez même choisir de ne pas vous fédérer aux autres.
            </>
        ),
    },
];

function Feature({ Svg, title, description }) {
    return (
        <div className={clsx('col col--4')}>
            <div className="text--center">
                <Svg className={styles.featureSvg} alt={title} />
            </div>
            <div className="text--center padding-horiz--md">
                <h3>{title}</h3>
                <p>{description}</p>
            </div>
        </div>
    );
}

export default function HomepageFeatures() {
    return (
        <section className={styles.features}>
            <div className="container">
                <div className="row">
                    {FeatureList.map((props, idx) => (
                        <Feature key={idx} {...props} />
                    ))}
                </div>
            </div>
        </section>
    );
}
